/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * ---------------------------------------------------------------------
 */


#include <sys/types.h>
#include <sys/queue.h>

#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "kv.h"


/*
 * create and initialise a keyvalue store
 */
kvstorep
kvstore_create(size_t keysz, size_t valuesz)
{
        kvstorep    kvstr;

        kvstr = calloc(1, sizeof kvstr);
        if (NULL == kvstr) {
                warn("could not allocate memory for key-value store");
                return kvstr;
        }

        kvstr->kvs = calloc(1, sizeof kvstr->kvs);
        if (NULL == kvstr->kvs) {
                free(kvstr);
                kvstr = NULL;
                warn("failed to allocate memory for tail queue");
                return kvstr;
        }

        TAILQ_INIT(kvstr->kvs);
        kvstr->keysz = keysz;
        kvstr->valuesz = valuesz;
        kvstr->entries = 0;
        return kvstr;
}


/*
 * create a key-value pair
 */
int
kvstore_add(kvstorep kvstr, const char *key, const char *value)
{
        struct keyvalue *kv;
        int             error = -1; 

        if (kvstr == NULL || key == NULL || value == NULL)
                return error;
        assert(0 == kvstore_del(kvstr, key));
        if (NULL == (kv = calloc(1, sizeof kv))) {
                warn("failed to allocate memory for key-value pair");
                return error;
        } 

        kv->key = calloc(kvstr->keysz, sizeof(kv->key));
        kv->value = calloc(kvstr->valuesz, sizeof(kv->value));
        if (NULL == kv->key || NULL == kv->value) {
                warn("failed to allocate memory for key-value pair");
                free(kv->key);
                free(kv->value);
                kv->key = NULL;
                kv->value = NULL;
                free(kv);
                kv = NULL;
                return error;
        }
        strlcpy(kv->key, key, kvstr->keysz);
        strlcpy(kv->value, value, kvstr->valuesz);
        TAILQ_INSERT_HEAD(kvstr->kvs, kv, kvps);
        kvstr->entries++;
        error = 0;

        return error;
}


/*
 * search the key-value store for the value for a given key, or
 * NULL if it can't be found. do not free the returned value.
 */
char *
kvstore_get(kvstorep kvstr, const char *key)
{
        struct keyvalue *kv;
        size_t          i;
        int             ok;
        char            *value;

        value = NULL;
        ok = 0;
        if (kvstr == NULL)
                return value;
        for (i = 0; i < kvstr->keysz; ++i) {
                if (key[i] == '\0') {
                        ok = 1;
                        break;
                }
        }
        if (!ok)
                return value;

        TAILQ_FOREACH(kv, kvstr->kvs, kvps) {
                if (0 == strncmp(kv->key, key, kvstr->keysz + 1)) {
                        value = kv->value;
                        break;
                }
        }
        return value;
}


/*
 * return the values in the kvstore
 */
char **
kvstore_keys(kvstorep kvstr)
{
        struct keyvalue *kv;
        char            **keys;
        size_t          i;
        int             invalid;

        invalid = 0;
        keys = NULL;
        if (kvstr == NULL)
                return keys;
        keys = calloc((kvstr->entries) + 1, sizeof keys);
        if (keys == NULL)
                return keys;

        keys[kvstr->entries + 1] = NULL;
        for (i = 0; i < (kvstr->entries + 1); i++) {
                keys[i] = NULL;
                keys[i] = calloc(kvstr->keysz, sizeof keys[i]);
                if (NULL == keys[i]) {
                        invalid = 1;
                        break;
                }
        }
        if (!invalid) {
                i = 0;
                printf("[-] kvstr->keysz: %u\n",
                    (unsigned int)kvstr->keysz);;
                TAILQ_FOREACH(kv, kvstr->kvs, kvps)
                        strlcpy(keys[i++], kv->key, kvstr->keysz);
        } else {
                for (i = 0; i < kvstr->entries; i++) {
                        free(keys[i]);
                        keys[i] = NULL;
                }
        }

        return keys;
}


/*
 * remove the key-value associated with a key
 */
int
kvstore_del(kvstorep kvstr, const char *key)
{
        struct keyvalue *kv;
        int             error = 0;

        if (NULL == kvstr || NULL == key)
                return 0;
        TAILQ_FOREACH(kv, kvstr->kvs, kvps) {
                if (NULL == kv->key)
                        break;
                if (0 == strncmp(kv->key, key, kvstr->keysz)) {
                        kvstore_delkv(kvstr, kv);
                        error = 0;
                        break;
                }
        }
        return error;
}


/*
 * free up a key-value structure.
 */
int
kvstore_delkv(kvstorep kvstr, struct keyvalue *kv)
{
        int     error = -1; 

        if (NULL == kvstr)
                return 0;
        free(kv->key);
        free(kv->value);
        kv->key = NULL;
        kv->value = NULL;
        TAILQ_REMOVE(kvstr->kvs, kv, kvps);
        free(kv);
        kv = NULL;
        kvstr->entries--;
        error = 0;

        return error;
}


/*
 * destroy a key-value store
 */
int
kvstore_destroy(kvstorep kvstr)
{
        struct keyvalue *kv;
        int             error = -1;

        while ((kv = TAILQ_FIRST(kvstr->kvs))) {
                if (-1 == kvstore_delkv(kvstr, kv))
                       return error; 
        }

        free(kvstr->kvs);
        kvstr->kvs = NULL;
        free(kvstr);
        kvstr = NULL;
        error = 0;
        return error;
}


