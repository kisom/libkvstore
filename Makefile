CC = gcc
AR = ar
CFLAGS += -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align
CFLAGS += -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations
CFLAGS += -Wnested-externs -Winline -Wno-long-long -O2 -ansi
CFLAGS += -Wstrict-prototypes -Werror -g
OBJS = kv.o
TESTOBJS = kvtest.o
LIB = libkvstore.a
TEST = kvtest
TESTLD = -L. -lkvstore
MANPAGE = ${LIB:R}.3

all: $(LIB) man

$(LIB): $(OBJS)
	$(AR) -rcs $@ $?

test: $(TEST)
	
$(TEST): $(TESTOBJS) $(LIB)
	$(CC) $(LDFLAGS) -o $@ $(TESTOBJS) $(TESTLD)

man: $(MANPAGE)
	mandoc -Tlint $(MANPAGE)
clean:
	-rm -f $(OBJS) $(LIB) *.core $(TEST) $(TESTOBJS)

.PHONY: all clean test
