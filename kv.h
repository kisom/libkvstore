/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR  
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */

#ifndef __LIBKVSTORE_KV_H
#define __LIBKVSTORE_KV_H

#include <sys/types.h>
#include <sys/queue.h>
#include <stdlib.h>


struct keyvalue {
        char                    *key;
        char                    *value;
        TAILQ_ENTRY(keyvalue)   kvps;
};
TAILQ_HEAD(tq_keyvalue, keyvalue);
typedef struct tq_keyvalue * tq_keyvaluep;

struct kvstore {
        tq_keyvaluep    kvs;
        size_t          entries;
        size_t          keysz;
        size_t          valuesz;
};
typedef struct kvstore * kvstorep;


kvstorep        kvstore_create(size_t, size_t);
int             kvstore_add(kvstorep, const char *, const char *); 
char            *kvstore_get(kvstorep, const char *);
char            **kvstore_keys(kvstorep);
int             kvstore_del(kvstorep, const char *);
int             kvstore_delkv(kvstorep, struct keyvalue *); 
int             kvstore_destroy(kvstorep);

#endif
