/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * ---------------------------------------------------------------------
 */


#include <sys/types.h>
#include <sys/queue.h>

#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include "kv.h"

#define TEST_KV_KEYSZ   (size_t)32
#define TEST_KV_VALUESZ (size_t)32
#define TEST_KV_TESTELM (size_t)12

static int addkey(kvstorep, const char *, const char *);
static int test_kvstore(void);

static int addkey(kvstorep kvstr, const char *key, const char *val)
{
        printf("\t[*] %s->%s\n", key, val);
        return kvstore_add(kvstr, key, val);
}

static int
test_kvstore()
{
        kvstorep    kvstr;
        int         error;

        printf("[+] creating kvstore\n");
        kvstr = kvstore_create(TEST_KV_KEYSZ, TEST_KV_VALUESZ);
        assert(kvstr != NULL);

        printf("[+] adding keys\n");
        assert(0 == addkey(kvstr, "hello", "world"));
        assert(0 == addkey(kvstr, "foo", "bar"));
        assert(0 == addkey(kvstr, "baz", "quux"));
        assert(0 == addkey(kvstr, "spam", "eggs"));
        assert(0 == addkey(kvstr, "monty", "python"));
        assert(0 == addkey(kvstr, "c", "rules"));
        assert(0 == addkey(kvstr, "puffy", "phoenix"));
        assert(0 == addkey(kvstr, "sherlock", "holmes"));
        assert(0 == addkey(kvstr, "miss", "kittin"));
        assert(0 == addkey(kvstr, "radio", "head"));
        assert(0 == addkey(kvstr, "portis", "head"));
        assert(0 == addkey(kvstr, "massive", "attack"));
        printf("[+] keys added\n");

        assert(kvstr->entries == TEST_KV_TESTELM);
        printf("[+] overwriting key\n");
        kvstore_add(kvstr, "c", "is fun");
        assert(kvstr->entries == TEST_KV_TESTELM);
        printf("[+] destroy kvstore\n");
        error = kvstore_destroy(kvstr);
        assert(error == 0);
        return error;
}

int
main(void)
{
        printf("[+] beginning test...\n");
        assert(test_kvstore() == 0);
        printf("[+] test successful!\n");
        return 0;
}
